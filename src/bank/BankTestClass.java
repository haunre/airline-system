/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package bank;

/**
 *
 * @author Henry
 */
public class BankTestClass {
     public static void main(String[] args){
         
         Bank bank = new Bank("RBC");
         Employees emp1 = new Employees("josh");
         Employees emp2 = new Employees("Andrew");
         Employees emp3 = new Employees("Linda");
         
         System.out.println("Employees " + emp1.getEmployeeName() + emp2.getEmployeeName() + emp3.getEmployeeName() + "all work for the " + bank.getName());
     }
    
}
